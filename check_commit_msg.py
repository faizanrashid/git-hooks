#!/usr/bin/python

import sys, os
from subprocess import call, Popen, PIPE

if os.environ.get('EDITOR'):
    editor = os.environ['EDITOR']
else:
     editor = "vim"

message_file = sys.argv[1]
sys.stdin = open('/dev/tty')
def check_format_rules(line_num, line):
    if line_num == 0:
        if len(line) > 60:
            return "Error: First line should be less than 60 characters in length."

    elif line_num == 1:
        if line and line[0] != "#":
           return "Error: Second line should be empty."

    if len(line) > 72:
	return "Error: Line %s should be less than 72 characters in length." % str(line_num+1)

    return False

while True:
    errors = []
    commit_msg = []
    with open(message_file) as msg_file:
         for line_num, line in enumerate(msg_file):
            stripped_line = line.strip()
            commit_msg.append(line)
            e = check_format_rules(line_num, stripped_line)
            if e:
                errors.append(e)

    if errors:
	with open(message_file, "r+") as fix_commit:
	    for line in commit_msg:
	        fix_commit.write(line)
       	    
	    fix_commit.write("#GIT COMMIT MESSAGE ERRORS:\n")
	    for error in errors:
		fix_commit.write("#	%s\n" % error)
 
        re_edit = raw_input("Invalid git commit message format.  Press y to edit and n to cancel the commit. [y/n]")
        if re_edit.lower() in ('n','no'):
            sys.exit(1)
        call('%s %s' % (editor, message_file), shell=True)
        continue	    
    break
